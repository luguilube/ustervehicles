package com.example.microservices.app.vehicles.uster.vehicles.models.services;

import com.example.microservices.app.vehicles.uster.vehicles.models.entities.Vehicle;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface VehicleService {
    public List<Vehicle> findAll();
    public Page<Vehicle> findAll(Pageable pageable);
    public Vehicle save(Vehicle vehicle);
    public Vehicle findById(Long id);
    public void deleteById(Long id);
}
