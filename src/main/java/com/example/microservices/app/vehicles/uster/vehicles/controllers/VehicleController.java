package com.example.microservices.app.vehicles.uster.vehicles.controllers;

import com.example.microservices.app.vehicles.uster.vehicles.models.entities.Vehicle;
import com.example.microservices.app.vehicles.uster.vehicles.models.services.VehicleService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.Map;

@RestController
public class VehicleController {
    private static final Logger log = LoggerFactory.getLogger(VehicleController.class);

    private final VehicleService service;

    @Autowired
    public VehicleController(VehicleService service) {
        this.service = service;
    }

    @GetMapping("/normal-list")
    public ResponseEntity<?> normalList() {
        try {
            return ResponseEntity.ok().body(service.findAll());
        } catch (InternalError e) {
            log.error("InternalError {}", e);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        } catch (Exception e1) {
            log.error("Exception {}", e1);
            return ResponseEntity.status(HttpStatus.SERVICE_UNAVAILABLE).build();
        }
    }

    @GetMapping("/list")
    public ResponseEntity<?> list(Pageable pageable) {
        try {
            return ResponseEntity.ok().body(service.findAll(pageable));
        } catch (InternalError e) {
            log.error("InternalError {}", e);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        } catch (Exception e1) {
            log.error("Exception {}", e1);
            return ResponseEntity.status(HttpStatus.SERVICE_UNAVAILABLE).build();
        }
    }

    @PostMapping("store")
    public ResponseEntity<?> store(@Valid @RequestBody Vehicle vehicle, BindingResult result) {
        try {
            if (result.hasErrors()) {
                return validation(result);
            }

            return ResponseEntity.status(HttpStatus.CREATED).body(vehicle);
        } catch (InternalError e) {
            log.error("InternalError {}", e);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        } catch (Exception e1) {
            log.error("Exception {}", e1);
            return ResponseEntity.status(HttpStatus.SERVICE_UNAVAILABLE).build();
        }
    }

    @GetMapping("/get/{id}")
    public ResponseEntity<?> get(@PathVariable Long id) {
        try {
            Vehicle vehicle = service.findById(id);

            if (vehicle == null) {
                return ResponseEntity.notFound().build();
            }

            return ResponseEntity.ok().body(vehicle);
        } catch (InternalError e) {
            log.error("InternalError {}", e);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        } catch (Exception e1) {
            log.error("Exception {}", e1);
            return ResponseEntity.status(HttpStatus.SERVICE_UNAVAILABLE).build();
        }
    }

    @DeleteMapping("delete/{id}")
    public ResponseEntity<?> delete(@PathVariable Long id) {
        try {
            service.deleteById(id);

            return ResponseEntity.noContent().build();
        } catch (InternalError e) {
            log.error("InternalError {}", e);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        } catch (Exception e1) {
            log.error("Exception {}", e1);
            return ResponseEntity.status(HttpStatus.SERVICE_UNAVAILABLE).build();
        }
    }

    protected ResponseEntity<?> validation(BindingResult result) {
        Map<String, Object> errors = new HashMap<>();

        result.getFieldErrors().forEach(err -> {
            errors.put(err.getField(), "El campo " + err.getField() + " " + err.getDefaultMessage());
        });

        return ResponseEntity.badRequest().body(errors);
    }
}
