package com.example.microservices.app.vehicles.uster.vehicles.models.repositories;

import com.example.microservices.app.vehicles.uster.vehicles.models.entities.Vehicle;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;
import java.util.Optional;

public interface VehicleRepository extends PagingAndSortingRepository<Vehicle, Long> {
    public List<Vehicle> findAll();
    public Page<Vehicle> findAll(Pageable pageable);
    public Vehicle save(Vehicle vehicle);
    public Optional<Vehicle> findById(Long id);
    public void deleteById(Long id);
}
