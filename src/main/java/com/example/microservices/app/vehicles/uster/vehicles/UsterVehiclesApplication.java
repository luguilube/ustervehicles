package com.example.microservices.app.vehicles.uster.vehicles;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@EnableEurekaClient
@SpringBootApplication
public class UsterVehiclesApplication {

	public static void main(String[] args) {
		SpringApplication.run(UsterVehiclesApplication.class, args);
	}

}
